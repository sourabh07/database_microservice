package com.micro.database.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.micro.database.dto.LocationDTO;
import com.micro.database.dto.StockDTO;
import com.micro.database.dto.UserDetailsDTO;
import com.micro.database.service.UserService;

@RestController
@RequestMapping("/back")
public class UserAPI {

	@Autowired
	UserService service;
	
	@RequestMapping (value = "/{userName}",method = RequestMethod.GET)
	UserDetailsDTO getUserDetails(@PathVariable("userName") final String userName)
	{
		
		return service.getUser(userName);
	}
	
	@RequestMapping (value = "/all",method = RequestMethod.GET)
	List<String> getAllUsers()
	{
		
		return service.getAllUsers();
	}
	
	@RequestMapping (value = "/authenticate",method = RequestMethod.POST)
	UserDetailsDTO getUserAuthentication(@RequestBody final UserDetailsDTO userDetailsDTO)
	{
		return service.authenticateUser(userDetailsDTO); 
	}
	
	@RequestMapping (value = "/add",method = RequestMethod.POST)
	Boolean addUserDetails(
			@RequestBody final UserDetailsDTO userDetailsDTO)
	{
		
		return service.addUser(userDetailsDTO);
	}
	
	@RequestMapping (value = "/put",method = RequestMethod.POST)
	Boolean updateUserDetails(
			@RequestBody final UserDetailsDTO userDetailsDTO)
	{
		
		return service.addUser(userDetailsDTO);
	}
	
	/*@RequestMapping (value = "/putStock",method = RequestMethod.POST)
	Boolean updateUserStocks(
			@RequestBody final UserDetailsStockDTO userDetailsStockDTO)
	{
		
		UserDetailsDTO user = service.getUser(userDetailsStockDTO.getFirstName()); 
		user.setStocks(new ArrayList<StockDTO>());
		user.setStocks(userDetailsStockDTO.getStocks());
		return service.addUser(user);
	}
	
	@RequestMapping (value = "/putLocation",method = RequestMethod.POST)
	Boolean updateUserStocks(
			@RequestBody final UserDetailsLocationDTO userDetailsLocationDTO)
	{
		
		UserDetailsDTO user = service.getUser(userDetailsLocationDTO.getFirstName()); 
		user.setLocations(new ArrayList<LocationDTO>());
		user.setLocations(userDetailsLocationDTO.getLocations());
		return service.addUser(user);
	}
	*/
	@RequestMapping (value = "/stock/{userName}",method = RequestMethod.GET)
	List<StockDTO> getUserStockDetails(@PathVariable("userName") final String userName)
	{
		
		return service.getUserStocks(userName);
	}
	
	@RequestMapping (value = "/location/{userName}",method = RequestMethod.GET)
	List<LocationDTO> getUserLocationDetails(@PathVariable("userName") final String userName)
	{
		
		return service.getUserLocations(userName);
	}
	
}
