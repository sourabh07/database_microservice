package com.micro.database.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.micro.database.dto.LocationDTO;
import com.micro.database.dto.StockDTO;
import com.micro.database.dto.UserDetailsDTO;
import com.micro.database.entity.LocationEntity;
import com.micro.database.entity.StockEntity;
import com.micro.database.entity.UserEntity;
import com.micro.database.repository.LocationRepository;
import com.micro.database.repository.StockRepository;
import com.micro.database.repository.UserRepository;
import com.micro.database.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userR;
	
	@Autowired
	LocationRepository locationR;
	
	@Autowired
	StockRepository stockR;
	
	@Override
	public UserDetailsDTO getUser(String userName) {
		
		ModelMapper mapper = new ModelMapper();
		return mapper.map(userR.findByFirstName(userName) == null ? new UserEntity():userR.findByFirstName(userName), UserDetailsDTO.class);
	}
	
	@Override
	public UserDetailsDTO authenticateUser(UserDetailsDTO userDetailsDTO) {
		
		ModelMapper mapper = new ModelMapper();
		UserEntity user = userR.findByFirstName(userDetailsDTO.getFirstName());
		if(userDetailsDTO.getPassword().equals(user.getPassword()))
			return mapper.map(user == null ? new UserEntity():user, UserDetailsDTO.class);
		return new UserDetailsDTO();
	}

	@Override
	public List<LocationDTO> getUserLocations(String userName) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(
				userR.findByFirstName(userName) == null ? 
							new ArrayList<LocationDTO>() : 
						userR
							.findByFirstName(userName)
							.getLocations(),new TypeToken<List<LocationDTO>>() {}.getType());
		}

	@Override
	public List<StockDTO> getUserStocks(String userName) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(
				
				userR.findByFirstName(userName) == null ? 
						new ArrayList<StockDTO>() :
				userR
					.findByFirstName(userName)
					.getStocks(),new TypeToken<List<StockDTO>>() {}.getType());
	}

	@Override
	public Boolean addUser(UserDetailsDTO userDetailsDTO) {
		ModelMapper mapper = new ModelMapper();
		
		System.out.println(userDetailsDTO.toString());
		
		UserEntity user = new UserEntity();
		mapper.map(userDetailsDTO, user);
		user.setLocations(mapper.map(userDetailsDTO.getLocations(), new TypeToken<List<LocationEntity>>() {}.getType()));
		user.setStocks(mapper.map(userDetailsDTO.getStocks(), new TypeToken<List<StockEntity>>() {}.getType()));
		
		System.out.println(user);
		
		if(!userR.save(user).equals(new UserEntity()))
			return Boolean.TRUE;
		else
			return Boolean.FALSE;
	}

	@Override
	public List<String> getAllUsers() {

		List<UserEntity> users = new ArrayList<UserEntity>();
		userR.findAll().forEach(users :: add);
		List<String> nameString = new ArrayList<String>();

		
		for (UserEntity user : users) {
			String name = user.getFirstName() + " " + user.getLastName();
			nameString.add(name);
			name = new String();
		}
		return nameString;
	}
}
