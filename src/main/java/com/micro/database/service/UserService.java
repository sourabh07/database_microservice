package com.micro.database.service;

import java.util.List;

import com.micro.database.dto.LocationDTO;
import com.micro.database.dto.StockDTO;
import com.micro.database.dto.UserDetailsDTO;

public interface UserService {

	
	UserDetailsDTO getUser(String userName);
	
	List<String> getAllUsers();
	
	List<LocationDTO> getUserLocations(String userName);
	
	List<StockDTO> getUserStocks(String userName);

	Boolean addUser(UserDetailsDTO userDetailsDTO);

	UserDetailsDTO authenticateUser(UserDetailsDTO userDetailsDTO);	
}
