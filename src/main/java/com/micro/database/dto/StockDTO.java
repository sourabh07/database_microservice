package com.micro.database.dto;

public class StockDTO {
	
	private String stockName;
	
	public String getStockName() {
		return stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
}
