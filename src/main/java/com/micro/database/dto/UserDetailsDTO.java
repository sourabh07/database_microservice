package com.micro.database.dto;

import java.util.List;

public class UserDetailsDTO {
	
	private String firstName;
	private String lastName;
	private String password;
	private List<StockDTO> stocks;
	private List<LocationDTO> locations;

	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<StockDTO> getStocks() {
		return stocks;
	}

	public void setStocks(List<StockDTO> stocks) {
		this.stocks = stocks;
	}

	public List<LocationDTO> getLocations() {
		return locations;
	}

	public void setLocations(List<LocationDTO> locations) {
		this.locations = locations;
	}

	@Override
	public String toString() {
		return "UserEntity [firstName=" + firstName + ", lastName=" + lastName + ", stocks=" + stocks
				+ ", locations=" + locations + "]";
	}
	
}
