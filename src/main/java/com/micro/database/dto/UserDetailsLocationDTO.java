package com.micro.database.dto;

import java.util.List;

public class UserDetailsLocationDTO {
	private String firstName;
	private String lastName;
	private List<LocationDTO> locations;

	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<LocationDTO> getLocations() {
		return locations;
	}

	public void setLocations(List<LocationDTO> locations) {
		this.locations = locations;
	}

	@Override
	public String toString() {
		return "UserDetailsLocationDTO [firstName=" + firstName + ", lastName=" + lastName + ", locations=" + locations
				+ "]";
	}

	
}
