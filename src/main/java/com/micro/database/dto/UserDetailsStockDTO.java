package com.micro.database.dto;

import java.util.List;

public class UserDetailsStockDTO {

	private String firstName;
	private String lastName;
	private List<StockDTO> stocks;
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<StockDTO> getStocks() {
		return stocks;
	}

	public void setStocks(List<StockDTO> stocks) {
		this.stocks = stocks;
	}

	@Override
	public String toString() {
		return "UserDetailsStockDTO [firstName=" + firstName + ", lastName=" + lastName + ", stocks=" + stocks + "]";
	}

	
}
