package com.micro.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.micro.database.repository.UserRepository;

@SpringBootApplication
@EnableAutoConfiguration
@EnableEurekaClient
@EnableWebMvc
public class DatabaseApplication {

	@Autowired
	UserRepository repo;
	
	public static void main(String[] args) {
		SpringApplication.run(DatabaseApplication.class, args);
	}
	/*
	public DatabaseApplication() {
		System.out.println(repo.count());
	}
	*/
	
	

}

