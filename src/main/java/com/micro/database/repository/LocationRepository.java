package com.micro.database.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.micro.database.entity.LocationEntity;

@Repository
public interface LocationRepository extends CrudRepository<LocationEntity, Integer> {

	
	
}
