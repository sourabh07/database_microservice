package com.micro.database.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.micro.database.entity.StockEntity;

@Repository
public interface StockRepository extends CrudRepository<StockEntity, Integer> {

}